from django.db import models

# Create your models here.
class UserDetails(models.Model):
    name=models.CharField(max_length=100,default='',blank=True)
    dp=models.ImageField(upload_to='images/',default='')
    info=models.FileField(upload_to='files/',default='')